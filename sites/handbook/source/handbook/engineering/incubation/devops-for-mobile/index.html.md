---
layout: handbook-page-toc
title: DevOps for Mobile Apps Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Mission

Our goal is to improve the experience for Developers targeting mobile platforms by providing CI/CD capabilities and workflows that will enhance the experience of provisioning and deploying mobile apps to iOS and Android devices. Our current focus combines the [Product Direction - DevOps for Mobile Applications ](https://about.gitlab.com/direction/mobile/mobile-devops/) and findings from our recent research.

## Our Hypothesis

Mobile software teams can see the same benefits as other software teams when adopting DevOps practices, but mobile teams are underserved in this area for a variety of reasons.

We believe that mobile teams are looking to adopt DevOps practices, but due to unfamiliar tooling and technical complexity, these initiatives can get time-consuming, expensive, and may not reach their full potential.

Our hypothesis is that we can improve the adoption of DevOps practices by mobile teams by providing opinionated tooling that is easy to use and doesn't require in-depth knowledge of tools unfamiliar to mobile developers like Docker, YAML, etc.

## Vision

Our current focus is around making the `build` > `test` > `release` process as simple as possible for mobile teams using GitLab by focusing on the following areas:

### Code Signing

Mobile Code Signing is one of the most confusing and error-prone parts of the Mobile DevOps process. We can eliminate much of the confusion and trial and error involved in getting pipelines set up by providing a system that makes it easy for developers to securely upload, visualize, and manage their code signing files.

[Adding binary file support to CI variables](https://gitlab.com/gitlab-org/gitlab/-/issues/346290) through a new capability called Secure Files. Secure Files is a generalized solution to the challenge mobile teams are faced with when trying to manage keystores and provisioning profiles in GitLab. By generalizing this solution, we can support other use cases while being able to provide additional enhancements for the mobile-specific use cases.

[Secure Files UX Proposal](https://gitlab.com/gitlab-org/gitlab/-/issues/347149) describes the progressive enhancements we will add to the UI to support the generalized and specific mobile use cases.

[Android Keystore Generation](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/34) is an idea we would like to explore to generate Android keystores on GitLab, which would eliminate some of the manual steps required today.

### App Store / Play Store Integration

With the Code Signing process simplified, the next big area of complexity we will improve is the release process for internal, beta, and public releases. For both iOS and Android platforms, credential certificates and API keys need to be generated and configured correctly for the release process to work. We will provide capabilities to simplify the setup and configuration of these integrations, as well as tools to test these integrations without having to run a pipeline.

[App Store Connect API Login](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/36)

[Play Store API Login](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/35)


### Improved Review Apps for Mobile

While have made some improvements with [Review Apps for Mobile](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/15) there is more that can be done to support a broader range of mobile apps and devices.

[Integrations with emulator services like AWS Device Farm and BrowserStack](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/42) will expand support for testing on a wide range of devices. Since these device farms operate differently than traditional web apps, we will create additional capabilities within Review Apps to support the specific mobile device integrations.

Additionally, with the popularity of cross-platform development tools, [Multiple Url support for Review Apps](https://gitlab.com/gitlab-org/gitlab/-/issues/276905) will deliver a nice addition for teams that want to keep the pipelines in sync across multiple platforms.

### Mobile CI Templates

As we build up the foundational components to the DevOps for Mobile experience, we will also roll out additional CI templates focused on mobile use cases which will provide an [AutoDevOps-like](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/21) experience. In addition, leaning on [Fastlane](https://fastlane.tools/) will allow us to limit the complexity in the CI templates while still providing robust capabilities for mobile use cases.

## Competitive Landscape

There are several competitors in this space providing visual pipeline builders on top of their own CI/CD systems (Bitrise, Appcircle, Buddybuild, and Codemagic) and the more prominent players, including Visual Studio App Center, Firebase, and AWS Mobile Services.

## Who We Are

The DevOps for Mobile Apps SEG is a [Single-Engineer Group](https://about.gitlab.com/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](https://about.gitlab.com/handbook/engineering/incubation/).

## How We Work

As we explore the opportunities and challenges in this space, we will share weekly demos. These demos will be recorded and shared in the [Weekly Demos issue](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/7) (which can be subscribed to for notification updates) as well as in the table below:

| Date              | Topic | Video | Issue |
|-------------------|-------|-------|-------|
| March 4, 2022 | Sync Updates & Device Farm Research | [https://youtu.be/-NGp08P6QGE](https://youtu.be/-NGp08P6QGE) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/56](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/56) |
| February 24, 2022 | Secure Files Documentation | [https://youtu.be/tFqYerQaAEk](https://youtu.be/tFqYerQaAEk) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/54](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/54) |
| February 10, 2022 | Secure Files MVC Release  | [https://youtu.be/E3V7Bmi9tcY](https://youtu.be/E3V7Bmi9tcY) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/50](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/50) |
| January 21, 2022 | Secure Files Follow-ups | [https://youtu.be/2OUhlp95xMU](https://youtu.be/2OUhlp95xMU) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/47](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/47) |
| January 14, 2022 | DevOps for Mobile Apps Vision | [https://youtu.be/sA3Lt9P-XAo](https://youtu.be/sA3Lt9P-XAo) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/43](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/43) |
| January 5, 2022 | Iterating on Secure Files & Runner Integration Script| [https://youtu.be/eK3FUskHfdo](https://youtu.be/eK3FUskHfdo) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/41](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/41) |
| December 15, 2021 | Adding Secure Files Uploader / Downloader MR | [https://youtu.be/kRwo4aIHd2Q](https://youtu.be/kRwo4aIHd2Q) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/40](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/40) |
| December 3, 2021 | Secure Files | [https://youtu.be/O4PbeJnvwmY](https://youtu.be/O4PbeJnvwmY) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/39](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/39) |
| November 18, 2021 | Code Signing UX | [https://youtu.be/s2O-Ho4yKsw](https://youtu.be/s2O-Ho4yKsw) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/38](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/38) |
| November 8, 2021 | Code Signing in GitLab | [https://youtu.be/3LnIuxI68BI](https://youtu.be/3LnIuxI68BI) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/31](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/31) |
| November 1, 2021 | Code Signing and Beta Releases with Fastlane | [https://youtu.be/O3LOSClDuiI](https://youtu.be/O3LOSClDuiI) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/30](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/30) |
| October 21, 2021 | GitLab Unfiltered Mobile App | [https://youtu.be/WcS4zMoeGxk](https://youtu.be/WcS4zMoeGxk) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/26](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/26) |
| October 13, 2021 | GitLab Unfiltered Mobile App | [https://youtu.be/xKwXNEDLFXQ](https://youtu.be/xKwXNEDLFXQ) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/25](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/25) |
| September 23, 2021 | Review Apps for Mobile Future Iterations | [https://youtu.be/iq5GRlnXd4E](https://youtu.be/iq5GRlnXd4E) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/23](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/23) |
| September 17, 2021 | Review Apps for Mobile | [https://youtu.be/2bej-XonbCQ](https://youtu.be/2bej-XonbCQ) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/18](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/18) |
| September 11, 2021 | Review Apps for Mobile | [https://youtu.be/j15IU3d0fNg](https://youtu.be/j15IU3d0fNg) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/17](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/17) |
| September 3, 2021 | Appetize CLI gem | [https://youtu.be/V8rzqrWDpTw](https://youtu.be/V8rzqrWDpTw) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/16](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/16) |
| August 26, 2021   | Android Prototype | [https://youtu.be/4Ijx4_KOBZI](https://youtu.be/4Ijx4_KOBZI) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/13](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/13) |
| August 19, 2021   | Mac OS Runners | [https://youtu.be/96C9Xx7gzuM](https://youtu.be/96C9Xx7gzuM) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/12](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/12) |
| August 13, 2021   | Current State | [https://youtu.be/f4U_n-zJwYg](https://youtu.be/f4U_n-zJwYg) | [https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/6](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues/6) |

## How To Contribute

#### GitLab Issues

Please feel free to create issues or participate in discussions in our [issue board](https://gitlab.com/gitlab-org/incubation-engineering/devops-for-mobile-apps/readme/-/issues).

#### Slack

We can also be found in Slack at `#incubation-eng` (GitLab Internal)
