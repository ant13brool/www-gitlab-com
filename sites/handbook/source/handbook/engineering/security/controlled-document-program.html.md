---
layout: handbook-page-toc
title: "Controlled Document Program"
description: "GitLab deploys control activities through policies and standards that establish what is expected and procedures that put policies and standards into action."
---
 
## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc .hidden-md .hidden-lg}
 
# Controlled Document Program
 
## Purpose
 
The Controlled Documents Program focuses on the formalization and annual review of Controlled Documents in order to meet our regulatory requirements.
 
GitLab deploys control activities through policies and standards that establish what is expected and procedures that put policies and standards into action.
 
The purpose of this program is to ensure that there is consistency in developing and maintaining controlled documents at GitLab utilizing a hierarchal approach for managing legal and regulatory requirements. 
 
There are two types of documentation at GitLab:
 
1. Controlled Documents: Formal policies, standards and procedures.
1. Uncontrolled Documents: Informal runbooks, certain handbook pages, guidelines, blog posts, templates, etc.
 
## Scope
 
What is a Controlled Document - A controlled document is a document that must undergo formal review, formal approval and controlled modification.
This program applies to all controlled documents developed in support of GitLab's statutory, regulatory and contractual requirements. Uncontrolled documents are dynamic in nature and not in scope of this procedure.
 
Goal: 100% of controlled documents reviewed relied upon by Security Compliance.
 
#### List of Controlled Documents:
 
<details markdown="1">
 
| Document Name | Description | URL | Code Owners |                                        
| :----: | :--------------------------------------: | :----: |:----:  |                                                                                    
| Acceptable Use Policy | This policy specifies requirements related to the use of GitLab computing resources and data assets by GitLab team members so as to protect our customers, team members, contractors, company, and other partners from harm caused by both deliberate and inadvertent misuse. | https://about.gitlab.com/handbook/people-group/acceptable-use-policy/ | Security, Legal and PeopleOps |                                                       
| Access Management Policy | *Procedure | https://about.gitlab.com/handbook/engineering/security/access-management-policy.html | Security Assurance Management |
| Access Review Guidelines | This policy defines the importance of the User access review process as an important control activity required for internal and external IT audits, helping to minimize threats, and provide assurance of who has access to what. | https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/access-reviews.html | Security Compliance Team |
| Backup Policy | This policy is to document that our production databases are taken every 24 hours with continuous incremental data (at 60 sec intervals). | https://about.gitlab.com/handbook/engineering/infrastructure/production/#backups | Infrastructure Management Team |                                         
| Backup Recovery Testing | This project implements a backup testing pipeline. It's purpose is to detect whether or not the backup is actually restorable and in good shape. After all, what good is a backup that cannot be restored?  This is implemented in a CI pipeline that we kick off daily on a schedule. It can also be triggered manually at any time (parallel pipelines are supported). | https://gitlab.com/gitlab-com/gl-infra/gitlab-restore/postgres-gprd/blob/master/README.md | Infrastructure Management Team |
| Business Continuity Plan | A business continuity plan is an overall organizational program for achieving continuity of operations for business functions. Continuity planning addresses both information system restoration and implementation of alternative business processes when systems are compromised. | https://about.gitlab.com/handbook/business-technology/gitlab-business-continuity-plan/ | Information Technology Team |
| Change Management Policy | This policy is to manage changes in the operational environment with the aim of doing so (in order of highest to lowest priority) safely, effectively and efficiently. | https://about.gitlab.com/handbook/engineering/infrastructure/change-management/ | Infrastructure Management Team |                                                
| Controlled Documents Program | Deploying control activities through policies and standards that establish what is expected and procedures that put policies and standards into action ensuring there is consistency in developing and maintaining controlled documents at GitLab utilizing a hierarchal approach for managing legal and regulatory requirements. | https://about.gitlab.com/handbook/engineering/security/controlled-document-procedure.html | Security Assurance Management |
| Data Classification Policy | This policy defines data categories and provides a matrix of security and privacy controls for the purposes of determining the level of protection to be applied to GitLab data throughout its lifecycle. | https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html | Security Assurance Management |
| Data Protection Impact Assessment (DPIA) Policy | This policy ensures that our use of personal data is fully understood, that risks to the rights and freedoms of individuals resulting from the processing of personal data are carefully examined and that all appropriate measures are put in place to protect these rights throughout the lifecycle of the processing. DPIAs, in conjunction with the associated forms and guidance, should be used to ensure that our obligations and policies in this area are met. | https://about.gitlab.com/handbook/legal/privacy/dpia-policy/| Security Management |                             
| Data Team Policy and Standards | This policy documents how the data team delivers results that matter securing our data. | https://about.gitlab.com/handbook/business-ops/data-team/ | Data Team Management |                                                                
| Database Disaster Recovery Policy | This policy documents our disaster recovery for databases. | https://about.gitlab.com/handbook/engineering/infrastructure/database/disaster_recovery.html | Infrastructure Management Team |                         
| Disaster Recovery | | https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/library/disaster-recovery/index.md |  Infrastructure Management Team |
| Encryption Policy | This policy documents the encryption process in which data is securely encoded at rest and in transit to remain hidden from or inaccessible to unauthorized users to better protect private, proprietary and sensitive data and enhance the security of communication between client applications and servers. | https://about.gitlab.com/handbook/engineering/security/vulnerability_management/encryption-policy.html | Security Assurance Management |                            
| GCF Security Control Lifecycle | | https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/security-control-lifecycle.html | Security Compliance Management |
| GitLab Password Policy | This policy documents the process of constructing secure passwords and ensuring proper password management to protect GitLab information systems and other resources from unauthorized use based, in part, on the recommendations by NIST 800-63B. | https://about.gitlab.com/handbook/security/#gitlab-password-policy-guidelines | Security Assurance Management |                                                         
| GitLab Terms of Service | The page documents the terms of service when using GitLab | https://about.gitlab.com/terms/ | GitLab Legal |
| Information Security Management System (ISMS) | This policy documents the boundaries and objectives of GitLab's ISMS | https://about.gitlab.com/handbook/engineering/security/ISMS.html | Security Assurance Management |
| IT Help Team Policy and Standards | This policy documents IT Support responsibilities for onboarding and managing company assets. | https://about.gitlab.com/handbook/business-ops/employee-enablement/it-help/ | Business Technology Management |
| IT Ops Policy and Standards | This policy documents IT Operations responsibilities for onboarding and managing company assets. | https://about.gitlab.com/handbook/business-ops/employee-enablement/it-ops-team/ | Business Technology Management |
| Off-boarding Policy Guidelines | This document is an off-boarding step by step process that covers all the steps necessary to successfully part ways with an employee following their resignation or termination. When done well, a clear offboarding process ensures a smooth transition for both the company and the departing employee. | https://about.gitlab.com/handbook/offboarding/offboarding_guidelines/ | People Operations Management |                                                             
| Production Architecture | The GitLab.com core infrastructure is primarily hosted in Google Cloud Platform's (GCP) us-east1 region (see Regions and Zones)—and we use GCP iconography in our diagrams to represent GCP resources. We do have dependencies on other cloud providers for separate functions. Some of the dependencies are legacy fragments from our migration from Azure, and others are deliberate to separate concerns in the event of cloud provider service disruption.   This document does not cover servers that are not integral to the public facing operations of GitLab.com. | https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/ | Infrastructure Management Team |     
| Records Retention & Disposal Policy | Thi GitLab records retention and disposal standard lists the specific retention and secure disposal requirements for critical GitLab records. | https://about.gitlab.com/handbook/engineering/security/records-retention-deletion.html | Security Risk Management|     
| Risk Management Policy | This policy documents the Information Security Risk Management Program for performing risk analysis of information resources that store, process or transmit an organization's data.  The purpose of the Security Operational Risk Management (“StORM”) program at GitLab is to identify, track, and treat security operational risks in support of GitLab's organization-wide objectives. The Security Risk team utlizes these procedures to ensure that security risks that may impact GitLab's ability to achieve its customer commitments and operational objectives are effectively identified and treated. | https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/guidance/RM.1.05_risk_management.html | Security Risk Management |
| Security Compliance Observation Management Procedure | | https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/observation-management.html | Security Compliance Management |
| Security Incident Communications Plan Policy | This policy documents the communication response plan to map out the who, what, when, and how of GitLab in notifying and engaging with internal stakeholders and external customers on security incidents. This plan of action covers the strategy and approach for security events which have a ‘high’ or greater impact as outlined in GitLab’s risk scoring matrix. | https://about.gitlab.com/handbook/engineering/security/security-operations/sirt/security-incident-communication-plan.html | Security Management |
| Security Incident Response Guide | This Policy documents the responsibilities of all GitLab team members when responding to or reporting security incidents. | https://about.gitlab.com/handbook/engineering/security/security-operations/sirt/sec-incident-response.html | Security Management |
| Security Operations On-Call Guide (Major Incidents) | This policy documents how the Security Operations Team (SecOps) is collectively on-call 24/7/365, split into 12-hour shifts Monday to Friday and 48-hour coverage Saturday and Sunday. | https://about.gitlab.com/handbook/engineering/security/secops-oncall.html#major-incident-response-workflow | Security Assurance Management |
| Third Party Vendor Security Review Policy | This policy documents how security reviews are performed on new and renewing third party vendors that are requested through the procurement process and minimizing the risk associated with third party applications and services. | https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/third-party-risk-management.html | Security Risk Management |
| Vulnerability Management Policy Overview | This policy documents the recurring process of identifying, classifying, prioritizing, mitigating, and remediating vulnerabilities. This overview will focus on infrastructure vulnerabilities and the operational vulnerability management process designed to provide insight into our environments, leverage GitLab for vulnerability workflows, promote healthy patch management among other preventative best-practices, and remediate risk; all with the end goal to better secure our environments. | https://about.gitlab.com/handbook/engineering/security/vulnerability_management/#vulnerability-management-overview | Security Assurance Management |
 
</details>
 
 
## Roles & Responsibilities:
 
| Role  | Responsibility |
|-----------|-----------|
| Security Governance Team | Responsible for maintaining the program and conducting annual controlled documents review |
| Security Compliance Team | Responsible for implementing and maintaining Security Policies and oversight of supporting standards and procedures as part of ongoing continuous control monitoring |
| Security Assurance Management | Responsible for approving changes to this program |
| Control Owners | Responsible for defining and implementing procedures to support Security policies and standards |
 
Everyone at GitLab is welcomed and encouraged to submit an MR to create or suggest changes to controlled documents at any time.
 
## Procedure
 
### Definitions by Hierarchy
 
- Policy: A policy is a statement of intent and defines GitLab's goals, objectives and culture. Statutory, regulatory, or contractual obligations are commonly the root cause for a policy’s existence, as such policies are designed to be centrally managed at the organizational level (e.g. Security Compliance Team or Legal & Ethics Compliance Team).
- Standard: Standards are mandatory actions or rules that give formal policies support and direction. Standards may take the form of technical diagrams.
- Procedure: Procedures are detailed instructions to achieve a given policy and, if applicable, supporting standard. Procedures are decentralized and managed by process/control owners.
 
### Creation
At minimum, controlled documents should cover the following key topic areas:
 
- Purpose: Overview of why the controlled document is being implemented.
- Scope: What does the controlled document apply to.
- Roles & Responsibilities: Who is responsible for doing what. This should refer to departments or roles instead of specific individuals.
- Policy Statements, Standards or Procedure: The details.
- Exceptions: Define how exceptions to the controlled document will be tracked.
- References:  Procedure documents should map back to a governing policy or standard, and may relate to one or more procedures or other uncontrolled documentation.
 
### Publishing
Creation of, or changes to, controlled documents must be approved by management, or a formally designated representative, of the owning department as defined in the Code Owners file prior to publishing.
 
Most controlled documents will be published to our publicly facing [handbook](https://about.gitlab.com/handbook/), however if there is [non public data](/handbook/engineering/security/data-classification-standard.html) included in the documentation it should be published via an *internal facing only* mechanism, e.g. an internal GitLab project or internal only handbook page. Controlled documents should be accessible to all internal team members.
 
### Review
Controlled documents are required to be reviewed and approved on a minimum of an annual basis and may be updated ad hoc as required by business operations.
 
## Exceptions
Exceptions to controlled documents must be tracked and approved by the controlled document approver(s) via an auditable format. Exception process should be defined in each controlled document. 
 
Exceptions to this procedure will be tracked as per the [Information Security Policy Exception Management Process](/handbook/engineering/security/#information-security-policy-exception-management-process).
 
## References
* Parent Policy: [Information Security Policy](/handbook/engineering/security/)
* [SCF Compliance Controls](/handbook/engineering/security/security-assurance/security-compliance/guidance/compliance.html)
* [Data Classification Standard](/handbook/engineering/security/data-classification-standard.html)
* [Controlled Documents Procedure](https://about.gitlab.com/handbook/engineering/security/controlled-document-procedure.html)


 

